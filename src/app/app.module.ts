import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPremissionComponent } from './Components/add-premission/add-premission.component';
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ReactiveFormsModule } from '@angular/forms';
import { PremissionListComponent } from './Components/premission-list/premission-list.component';

const routes: Routes = [
  {
    path: 'PremissionsList',
    component: PremissionListComponent
  },
  {
    path: 'AddPremission',
    component: AddPremissionComponent

  }

];


@NgModule({
  declarations: [
    AppComponent,

    AddPremissionComponent,
    PremissionListComponent
  ],
  imports: [
    BrowserModule, InputTextModule, ButtonModule,
    SliderModule, MultiSelectModule,
    RouterModule.forRoot(routes, { enableTracing: true }),
    HttpClientModule,
    FormsModule,
    TableModule,
    BrowserAnimationsModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
