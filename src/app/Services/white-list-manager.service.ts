import { Injectable } from '@angular/core';
import { IPDetails } from '../Classes/ipdetails';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WhiteListManagerService {
  IpList: IPDetails[] = [];
  // BASIC_URL = "https://localhost:44380/api/WhiteListManager/";
  BASIC_URL = "/api/WhiteListManager/";
  constructor(private http: HttpClient) { }
  addIpPremission(IpDetails: IPDetails): Observable<any> {
    return this.http.post(this.BASIC_URL + "AddPremission", IpDetails);
  }
  UpdateIpPremission(ip: IPDetails) {
    return this.http.put(this.BASIC_URL + "UpdatePremission", ip)
  }
  DeleteIpPremission(ip: IPDetails) {
    debugger
    return this.http.delete(this.BASIC_URL + "DeletePremission/" + ip.id)
  }
  GetIpPremissionsList() {

    return this.http.get(this.BASIC_URL + "GetAllPremission")
  }
}
