import { TestBed } from '@angular/core/testing';

import { WhiteListManagerService } from './white-list-manager.service';

describe('WhiteListManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WhiteListManagerService = TestBed.get(WhiteListManagerService);
    expect(service).toBeTruthy();
  });
});
