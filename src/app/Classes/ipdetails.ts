export class IPDetails {
    /**
     *
     */
    constructor(
        public id: number,
        public ipAddress: string,
        public ipDescription: string
    ) {
    }

}
