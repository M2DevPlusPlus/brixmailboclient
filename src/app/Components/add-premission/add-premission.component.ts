import { Component, OnInit } from '@angular/core';
import { IPDetails } from 'src/app/Classes/ipdetails';
import { WhiteListManagerService } from 'src/app/Services/white-list-manager.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { isString } from 'util';

@Component({
  selector: 'app-add-premission',
  templateUrl: './add-premission.component.html',
  styleUrls: ['./add-premission.component.css']
})
export class AddPremissionComponent implements OnInit {

  constructor(public whiteListManagerService: WhiteListManagerService, public location: Location) { }
  newIpDetails: IPDetails
  premission: FormGroup
  get f() { return this.premission.controls; }
  REGEX = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
  ngOnInit() {
    document.getElementById('id01').style.display = 'block';
    this.premission = new FormGroup({
      ip: new FormControl('', [Validators.required, Validators.pattern(this.REGEX)]),
      desc: new FormControl('')
    })
  }
  addIpPremission() {
    debugger;
    this.newIpDetails = new IPDetails(0, this.f.ip.value, this.f.desc.value)
    this.whiteListManagerService.addIpPremission(this.newIpDetails).subscribe(
      data => {
      
          this.whiteListManagerService.IpList.push(this.newIpDetails)
      },
      err => {alert ("adding not allowed");this.location.back()},
      () => this.location.back()
    )
  }
}
