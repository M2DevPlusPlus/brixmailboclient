import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremissionListComponent } from './premission-list.component';

describe('PremissionListComponent', () => {
  let component: PremissionListComponent;
  let fixture: ComponentFixture<PremissionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremissionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremissionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
