import { Component, OnInit } from '@angular/core';
import { IPDetails } from 'src/app/Classes/ipdetails';
import { WhiteListManagerService } from 'src/app/Services/white-list-manager.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-premission-list',
  templateUrl: './premission-list.component.html',
  styleUrls: ['./premission-list.component.css']
})
export class PremissionListComponent implements OnInit {
  clonedIps: { [s: string]: IPDetails; } = {};
  IP_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
  load: boolean;
  deleting: boolean = false
  constructor(public wlservice: WhiteListManagerService, public location: Router) { }

  ngOnInit() {
    this.wlservice.GetIpPremissionsList().subscribe((data) => {
      console.log(data);
      this.wlservice.IpList = data as IPDetails[];
      this.load = true
    })

  }

  onRowEditInit(ip: IPDetails) {
    this.clonedIps[ip.id] = { ...ip };
  }


  onRowEditSave(ip: IPDetails) {
    if (ip.ipAddress) {
      delete this.clonedIps[ip.id];
      this.wlservice.UpdateIpPremission(ip).subscribe(
        data => console.log("EDIT SUCCESS"),
        err => console.log("edit error")


      )
    }
    else {
    }
  }
  Delete(ip: IPDetails) {
    this.deleting = false;
    debugger
    this.wlservice.DeleteIpPremission(ip).subscribe(data => { this.wlservice.IpList.splice(this.wlservice.IpList.indexOf(ip), 1) }
    )
  }
  onRowEditCancel(ip: IPDetails, index: number) {
    this.wlservice.IpList[index] = this.clonedIps[ip.id];
    delete this.clonedIps[ip.id];

  }
  add() {
    this.location.navigate(['/AddPremission'])
  }


}
